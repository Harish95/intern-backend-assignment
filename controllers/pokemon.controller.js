import * as pokemonService from "../services/pokemon.service.js";

const getPokemonList = async (req, res, next) => {
  try {
    const pokemon = await adminService.getPokemonList(req);
    // On Success, return list of pokemon
    return res.status(400).send(pokemon);
  } catch (error) {
    next(error);
  }
};

export { getPokemonList };

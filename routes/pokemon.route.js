import express from "express";

import * as pokemonController from "../controllers/pokemon.controller.js";

const router = express.Router();

router.get("/pokemon", pokemonController.getPokemonlist);

export default router;

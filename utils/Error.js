/* eslint-disable space-before-function-paren */
/* eslint-disable require-jsdoc */

class Error {
	constructor(message, statusCode) {
		this.message = message;
		this.statusCode = statusCode;
	}
}

export default Error;

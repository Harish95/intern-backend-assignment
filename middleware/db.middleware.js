import mongoose from 'mongoose';

const dbMiddleWare = (url) => {
	mongoose.connect(`${url}`, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	// Connect to DB
	const db = mongoose.connection;
	db.on('connected', () => {
		console.log('DB Connection Successful');
	});

	db.on('error', () => {
		console.error('DB Connection Failed');
	});
};

export default dbMiddleWare;

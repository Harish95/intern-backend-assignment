import express from "express";
import pokemonRoutes from "../routes/pokemon.route.js";
import errorHandler from "./error.middleware.js";

const routesMiddleWare = (app) => {
  app.use(express.json());
  app.use("/api", pokemonRoutes);

  // Error Handler middleware always comes at the end
  app.use(errorHandler);
};

export default routesMiddleWare;

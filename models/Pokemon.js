import mongoose from "mongoose";

const AbilitiesSchema = new mongoose.Schema({
  ability: {
    name: String,
    url: String,
  },
  is_hidden: Boolean,
  slot: Number,
});

const StatSchema = new mongoose.Schema({
  base_stat: Number,
  effort: Number,
  stat: {
    name: String,
    url: String,
  },
});

const SpriteSchema = new mongoose.Schema({
  "generation-i": {
    "red-blue": {
      back_default: String,
      back_gray: String,
      front_default: String,
      front_gray: String,
    },
    yellow: {
      back_default: String,
      back_gray: String,
      front_default: String,
      front_gray: String,
    },
  },
  "generation-ii": {
    crystal: {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
    gold: {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
    silver: {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
  },
  "generation-iii": {
    emerald: {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
    "firered-leafgreen": {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
    ruby: {
      back_default: String,
      back_shiny: String,
      front_default: String,
      front_shiny: String,
    },
  },
});

const PokemonSchema = new mongoose.Schema({
  id: Number,
  name: String,
  abilities: [AbilitiesSchema],
  height: Number,
  weight: Number,
  sprites: SpriteSchema,
  stats: [StatSchema],
});

const Pokemon = mongoose.model("Pokemon", PokemonSchema);

export { Pokemon };

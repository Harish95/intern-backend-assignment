import Error from "../utils/Error.js";

import { Pokemon } from "../models/Pokemon.js";

// Function will return all pokemon data in DB
const getPokemonList = async () => {
  try {
    const pokemon = await Pokemon.find();
    return pokemon;
  } catch (error) {
    throw new Error("Pokemon List could not be found", 400);
  }
};